package com.zealcomm.app.util

/**
 * Created by fuli.niu on 2017/1/3.
 */
object ExceptionUtil {
    @JvmStatic
    fun getExceptionTraceString(e: Exception?): String {
        if (null == e) {
            return "ExceptionTool.getExceptionTraceString's parameter is null"
        }
        val sb = StringBuffer()
        sb.append(e.toString())
        sb.append("\n")
        val stackTraceElements = e.stackTrace
        if (null != stackTraceElements && stackTraceElements.size > 0) {
            for (stackTraceElement in stackTraceElements) {
                if (null != stackTraceElement) {
                    sb.append("\t")
                    sb.append(stackTraceElement.toString())
                    sb.append("\n")
                }
            }
        }
        return sb.toString()
    }

    fun getExceptionTraceString(e: Exception?, showInLogcat: Boolean): String {
        if (showInLogcat) {
            e?.printStackTrace()
        }
        return getExceptionTraceString(e)
    }
}
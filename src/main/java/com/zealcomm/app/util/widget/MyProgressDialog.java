package com.zealcomm.app.util.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zealcomm.app.util.ExceptionUtil;
import com.zealcomm.app.util.LogUtil;
import com.zealcomm.app.util.R;
import com.zealcomm.app.util.StringUtil;

public class MyProgressDialog extends Dialog {

    private ImageView mLoadingImg;
    private TextView tvTip;
    private boolean canAutoClose = true;
    private static MyProgressDialog dialog;

    private MyProgressDialog(Context context) {
        this(context, true);
    }

    private MyProgressDialog(Context context, boolean canAutoClose) {
        super(context, R.style.dialog);
        dialog = this;
        this.canAutoClose = canAutoClose;
        // getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.process_dialog);
        mLoadingImg = findViewById(R.id.loadingimg);
        tvTip = findViewById(R.id.messagetv);
    }

    /**
     * 当没有消息时只展示转圈
     *
     * @param message
     */
    private void setMessage(String message) {
        new Handler().post(() -> changeTip(message));
    }

    private String getMessage() {
        return tvTip.getText().toString();
    }

    private void changeTip(String tip) {
        if (!StringUtil.isEmpty(tip)) {
            tvTip.setText(tip);
            tvTip.setVisibility(View.VISIBLE);
        } else {
            tvTip.setText("");
            tvTip.setVisibility(View.GONE);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
        mLoadingImg.startAnimation(animation);
    }

    public static void show(Activity activity) {
        showMessage(activity, null);
    }

    public static void showMessage(Activity activity, String message) {
        try {
            if (dialog == null) {
                dialog = new MyProgressDialog(activity);
                dialog.setMessage(message);
                dialog.show();
            } else {
                if (!dialog.isShowing()) {
                    dialog.setMessage(message);
                    dialog.show();
                }
            }
        } catch (Exception e) {
            LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
        }
    }

    public boolean isCanAutoClose() {
        return canAutoClose;
    }

    /***
     * 关闭
     */
    public static void dismissProgress() {
        try {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception e) {
            LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
        }

    }

    /***
     * 关闭
     */
    public static void dismissProgress2() {
        if (dialog != null) {
            String msg = dialog.getMessage();
            if (!StringUtil.isEmpty(msg) && msg.contains("上传中")) {
                // 上传文件的部分不要自动关闭
                return;
            } else {
                dialog.dismiss();
                dialog = null;
            }
        }
    }
}

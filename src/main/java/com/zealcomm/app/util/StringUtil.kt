package com.zealcomm.app.util

import java.util.*

object StringUtil {
    fun str2Asc(value: String): Long {
        val sbu = StringBuffer()
        val chars = value.toCharArray()
        for (i in chars.indices) {
            if (i != chars.size - 1) {
                sbu.append(chars[i].toInt())
            } else {
                sbu.append(chars[i].toInt())
            }
        }
        return sbu.toString().toLong()
    }

    fun urlParameters2Json(urlParameters: String): String {
        val keyValues = urlParameters.split("&").toTypedArray()
        var keyValue: Array<String?>
        val sb = StringBuffer()
        sb.append("{")
        for (str in keyValues) {
            keyValue = str.split("=").toTypedArray()
            sb.append("\"")
            sb.append(keyValue[0])
            sb.append("\":")
            sb.append("\"")
            sb.append(keyValue[1])
            sb.append("\",")
        }
        sb.deleteCharAt(sb.length - 1)
        sb.append("}")
        return sb.toString()
    }

    // 暂时这样写，以后再扩充
    fun isCellPhoneNumber(cellPhoneNumber: String?): Boolean {
        return if (null == cellPhoneNumber || cellPhoneNumber.length != 11) {
            false
        } else {
            true
        }
    }

    @JvmStatic
    fun isEmpty(string: String?): Boolean {
        return if (string == null) {
            true
        } else {
            if (string.lowercase(Locale.getDefault()) == "null") {
                true
            } else string.isEmpty()
        }
    }

    /**
     * parameters 至少匹配到了 keys 中的一个
     *
     * @param keys
     * @param parameters
     */
    fun matchOnceAtLeast(keys: Array<String?>?, vararg parameters: String?): Boolean {
        //LogUtil.i(Arrays.asList(keys) + "   " + Arrays.asList(parameters));
        if (keys != null) {
            // parameters 不会为空
            for (key in keys) {
                if (null == parameters) {
                    if (key == null) {
                        return true
                    }
                } else {
                    for (parameter in parameters) {
                        if (key == null) {
                            if (null == parameter) {
                                return true
                            }
                        } else {
                            if (key == parameter) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    fun isNotAllEmpty(vararg parameters: String?): Boolean {
        if (parameters != null && parameters.isNotEmpty()) {
            for (str in parameters) {
                if (!isEmpty(str)) {
                    return true
                }
            }
        }
        return false
    }

    fun hasEmptyValue(vararg values:String?):Boolean{
        if (values != null && values.isNotEmpty()){
            for (str in values){
                if (isEmpty(str)){
                    return true
                }
            }
            return false
        }
        return true
    }
}
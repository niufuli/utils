package com.zealcomm.app.util

import android.util.Log
import com.zealcomm.app.util.StringUtil.isEmpty

object LogUtil {

    @JvmStatic
    fun i(log: String?) {
        var log = log
        if (BuildConfig.DEBUG) {
            if (isEmpty(log)) {
                log = "由于日志为空，所以返回了这句"
            }
            Log.i("NFL", log!!)
        }
    }

    @JvmStatic
    fun e(log: String?) {
        var log = log
        if (BuildConfig.DEBUG) {
            if (isEmpty(log)) {
                log = "由于日志为空，所以返回了这句"
            }
            Log.e("NFL", log!!)
        }
    }

}
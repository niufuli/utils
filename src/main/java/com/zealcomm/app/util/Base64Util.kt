package com.zealcomm.app.util

import android.util.Base64
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

object Base64Util {
    fun imageToBase64ByPath(imgPath: String?): String {
        var `in`: InputStream? = null
        var data: ByteArray? = null
        try {
            `in` = FileInputStream(imgPath)
            data = ByteArray(`in`.available())
            `in`.read(data)
            `in`.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return Base64.encodeToString(data, Base64.DEFAULT)
    }

    fun decode(encodeString: String?): String? {
        try {
            return String(Base64.decode(encodeString, Base64.DEFAULT))
        } catch (e: Exception) {
            LogUtil.i(ExceptionUtil.getExceptionTraceString(e))
            return null
        }
    }
}
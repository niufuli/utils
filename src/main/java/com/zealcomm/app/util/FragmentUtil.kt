package com.zealcomm.app.util

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

object FragmentUtil {

    @JvmStatic
    fun addFragment(activity: FragmentActivity, fragment: Fragment?, @IdRes containerViewId: Int) {
        if (fragment != null) {
            activity.supportFragmentManager.beginTransaction()
                .replace(containerViewId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
        }
    }
    @JvmStatic
    fun addFragmentNotBackStack(
        activity: FragmentActivity,
        fragment: Fragment?,
        @IdRes containerViewId: Int
    ) {
        if (fragment != null) {
            activity.supportFragmentManager.beginTransaction()
                .replace(containerViewId, fragment, fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
        }
    }
    @JvmStatic
    fun showFragment(activity: FragmentActivity, fragment: Fragment?, @IdRes containerViewId: Int) {
        if (fragment != null && !fragment.isAdded) {
            activity.supportFragmentManager.beginTransaction()
                .add(containerViewId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
        } else {
            activity.supportFragmentManager.beginTransaction()
                .show(fragment!!)
                .commitAllowingStateLoss()
        }
    }
    @JvmStatic
    fun hideFragment(activity: FragmentActivity, fragment: Fragment?) {
        if (fragment != null) {
            activity.supportFragmentManager.beginTransaction()
                .hide(fragment)
                .commitAllowingStateLoss()
        }
    }
    @JvmStatic
    fun removeFragment(activity: FragmentActivity) {
        if (activity.supportFragmentManager.backStackEntryCount > 1) {
            activity.supportFragmentManager.popBackStack()
        } else {
            activity.finish()
        }
    }
}
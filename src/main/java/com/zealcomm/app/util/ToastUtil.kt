package com.zealcomm.app.util

import android.content.Context
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast

object ToastUtil {
    private var context: Context? = null
    fun init(context: Context) {
        ToastUtil.context = context.applicationContext ?: context
    }

    @JvmStatic
    fun showShort(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun showLong(msg: String?) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    fun showMiddleToast(tvStr: String?): Toast {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_my_toast, null)
        val tv = view.findViewById<View>(R.id.tv_toast) as TextView
        tv.gravity = Gravity.CENTER
        tv.text = if (TextUtils.isEmpty(tvStr)) "" else tvStr
        val toast = Toast.makeText(context, tvStr, Toast.LENGTH_SHORT)
        toast.view = view
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
        return toast
    }

    fun showTopToast(tvStr: String?): Toast {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_my_toast, null)
        val tv = view.findViewById<View>(R.id.tv_toast) as TextView
        tv.gravity = Gravity.CENTER
        tv.text = if (TextUtils.isEmpty(tvStr)) "" else tvStr
        val toast = Toast.makeText(context, tvStr, Toast.LENGTH_SHORT)
        toast.view = view
        toast.setGravity(Gravity.TOP, 0, 50)
        toast.show()
        return toast
    }

    fun showLongMiddleToast(tvStr: String?): Toast {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_my_toast, null)
        val tv = view.findViewById<View>(R.id.tv_toast) as TextView
        tv.gravity = Gravity.CENTER
        tv.text = if (TextUtils.isEmpty(tvStr)) "" else tvStr
        val toast = Toast.makeText(context, tvStr, Toast.LENGTH_LONG)
        toast.view = view
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
        return toast
    }
}
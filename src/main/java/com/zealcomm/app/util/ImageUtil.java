package com.zealcomm.app.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ImageUtil {

    private static File mPhotoFile = null;
    private static int LONG_PICTURE_MAX_HEIGHT = 400;

    public static void setPhotoFile(File photoFile) {
        mPhotoFile = photoFile;
    }

    public static File getPhotoFile() {

        return mPhotoFile;
    }

    /**
     * 保存图片到 Pictures 目录下，不刷新图库
     *
     * @param bmp           要变成图片的 bitmap
     * @param imageFileName 保存到本地的图片文件名，如：时间戳.jpg
     * @return 文件的真实地址
     */
    public static String onlySaveBitmap(Context context, Bitmap bmp, String imageFileName) {
        Context contextImpl = context.getApplicationContext() == null ? context : context.getApplicationContext();
        // 首先保存图片
        File appDir = contextImpl.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        // /storage/emulated/0/Android/data/com.longjiang.xinjianggong.enterprise/files/Pictures
        LogUtil.i("要保存的路径：" + appDir.getAbsolutePath());
        if (!appDir.exists()) {
            appDir.mkdirs();
        }
        String fileName = imageFileName + ".jpg";
        File file = new File(appDir, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setPhotoFile(file);
        return file.getAbsolutePath();
    }

    /*
     * 直接保存到系统相册，这个时候只会有一张图片
     */
    public static boolean saveBitmap(Context context, Bitmap bitmap, String imageFileName) {
        Context contextImpl = context.getApplicationContext() == null ? context : context.getApplicationContext();
        File appDir = contextImpl.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        // /storage/emulated/0/Android/data/com.longjiang.xinjianggong.enterprise/files/Pictures
        LogUtil.i("要保存的路径：" + appDir.getAbsolutePath());
        if (!appDir.exists()) {
            appDir.mkdirs();
        }
        // bitmap 最终保存到的位置
        File imageFile = new File(appDir, imageFileName);
        if (imageFile.exists()) {
            imageFile.delete();
        }
        FileOutputStream out;
        try {
            out = new FileOutputStream(imageFile);
            // 先将图片保存在 app 所属的空间下
            // 格式为 JPEG，照相机拍出的图片为JPEG格式的，PNG格式的不能显示在相册中
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)) {
                out.flush();
                out.close();
                ToastUtil.showShort("图片已保存到相册");
                // 尝试把 app 空间下的图片移动到 SdCard 下的 picture 下
                // SdCard 根目录
                String rootPath = appDir.getParentFile().getParentFile().getParentFile().getParentFile().getParentFile().getAbsolutePath();
                // SdCard 下的 Pictures 文件夹下建立名为 longjiang 的文件夹
                File pictureDir1 = new File(rootPath + File.separator + "Pictures" + File.separator + "longjiang");
                if (!pictureDir1.exists()) {
                    pictureDir1.mkdirs();
                }
                // 在 longjiang 文件夹下保存图片
                File fileTemp1 = new File(pictureDir1.getAbsolutePath(), imageFileName);
                // 将 app 空间下的图片移动到 sdcard 的 Pictures 中的 longjiang 目录下
                boolean removeSuccess = imageFile.renameTo(fileTemp1);
                if (removeSuccess) {
                    // 移动成功后刷新图片文件的路径信息
                    imageFile = fileTemp1;
                }
                LogUtil.i("文件写入到 sdcard 相册中：" + removeSuccess);
                // 最后通知图库更新
                // TODO 从 API 1 就有 MediaScannerConnection 似乎不需要区分版本，待验证
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    // 判断SDK版本是不是4.4或者高于4.4
                    // TODO 与使用 ContentValues 的区别是什么？会不会在 MediaStore 中插入数据？
                    MediaScannerConnection.scanFile(
                            contextImpl,
                            new String[]{imageFile.getAbsolutePath()},
                            new String[]{FileUtil.getMimeType(imageFile)},
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    LogUtil.i("图库更新完成1：" + path);
                                    LogUtil.i("图库更新完成2：" + uri.getPath());
                                }
                            });
                } else {
                    // TODO 这里如果不使用广播，而使用 ContentValues 在 4.4 以下是否可行 ？
                    final Intent intent;
                    if (imageFile.isDirectory()) {
                        intent = new Intent(Intent.ACTION_MEDIA_MOUNTED);
                        intent.setClassName("com.android.providers.media", "com.android.providers.media.MediaScannerReceiver");
                        intent.setData(Uri.fromFile(Environment.getExternalStorageDirectory()));
                    } else {
                        intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                        // TODO 4.4 以下 getImageContentUri 返回的时 content：// 还是 file:// ? 这里是否有效 ？
                        // 经测试 4.2 使用 content：// 是可以的
                        intent.setData(getImageContentUri(contextImpl, imageFile.getAbsolutePath()));
                        LogUtil.i("4.4以下触发这里:" + intent.getDataString());
                        // intent.setData(Uri.fromFile(file));
                    }
                    contextImpl.sendBroadcast(intent);
                }
            }
            return true;
        } catch (Exception e) {
            LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
        }
        return false;
    }

    /**
     * 这个经测试可以成功保存到相册
     *
     * @param view
     * @param imageFileName
     */
    public static boolean saveView(Context context, View view, String imageFileName) {
        return saveBitmap(context, convertViewToBitmap(view), imageFileName);
    }


    /**
     * 把 bitmap 保存到 Pictures 文件夹中，并通知图库更新最新状态
     */
    public static void saveBitmapAndRefreshGallery(Context context, Bitmap bmp, String picName) {
        // insertImage 后会在 Pictures 文件夹中保存这个 bitmap ， 不发送广播图库也会更新
//        String imageUriString =
        MediaStore.Images.Media.insertImage(context.getContentResolver(), bmp, picName, null);
//        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        Uri uri = Uri.parse(imageUriString) ;
//        intent.setData(uri);
//        context.sendBroadcast(intent);
        ToastUtil.showShort("图片保存成功");
    }

    /**
     * View转换为Bitmap图片
     *
     * @param view
     * @return Bitmap
     */
    public static Bitmap convertViewToBitmap(View view) {
        // 创建Bitmap,最后一个参数代表图片的质量.
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        // 创建Canvas，并传入Bitmap.
        Canvas canvas = new Canvas(bitmap);
        // View把内容绘制到canvas上，同时保存在bitmap.
        view.draw(canvas);
        return bitmap;
    }

    /**
     * 根据文件的真实路径获得对应的 uri 地址（解决 Android 10 没有访问权限的问题）
     *
     * @param path 文件的真实路径，用于筛选查询结果
     */
    public static Uri getImageContentUri(Context context, String path) {
        Context contextImpl = context.getApplicationContext() == null ? context : context.getApplicationContext();
        // content://media/external/images/media
        Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Images.Media._ID};
        String selection = MediaStore.Images.Media.DATA + "=? ";
        String[] selectionArgs = new String[]{path};
        String sortOrder = null;
        Cursor cursor = contextImpl.getContentResolver()
                .query(queryUri, projection, selection, selectionArgs, sortOrder);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            return Uri.withAppendedPath(queryUri, "" + id);
        } else {
            // 如果图片不在手机的共享图片数据库，就先把它插入。
            if (new File(path).exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, path);
                return contextImpl.getContentResolver().insert(queryUri, values);
            } else {
                return null;
            }
        }
    }

    public static void openAlbum(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);//图片列表
        activity.startActivity(intent);
    }

    public static void openVideoGallery(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(MediaStore.Video.Media.CONTENT_TYPE);
        activity.startActivity(intent);
    }

    public static void loadLongPicture(Bitmap resource, Bitmap.CompressFormat format, ImageView imageView) {
        int maxHeight = LONG_PICTURE_MAX_HEIGHT;
        Rect mRect = new Rect();
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            resource.compress(format, 100, baos);
            InputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            //BitmapRegionDecoder newInstance(InputStream is, boolean isShareable)
            //用于创建BitmapRegionDecoder，isBm表示输入流，只有jpeg和png图片才支持这种方式，
            // isShareable如果为true，那BitmapRegionDecoder会对输入流保持一个表面的引用，
            // 如果为false，那么它将会创建一个输入流的复制，并且一直使用它。即使为true，程序也有可能会创建一个输入流的深度复制。
            // 如果图片是逐步解码的，那么为true会降低图片的解码速度。如果路径下的图片不是支持的格式，那就会抛出异常
            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(isBm, true);
            final int imgWidth = decoder.getWidth();
            final int imgHeight = decoder.getHeight();
            BitmapFactory.Options opts = new BitmapFactory.Options();
            //计算图片要被切分成几个整块，
            // 如果sum=0 说明图片的长度不足3000px，不进行切分 直接添加
            // 如果sum>0 先添加整图，再添加多余的部分，否则多余的部分不足3000时底部会有空白
            int sum = imgHeight / maxHeight;
            int redundant = imgHeight % maxHeight;
            List<Bitmap> bitmapList = new ArrayList<>();
            //说明图片的长度 < maxHeight
            if (sum == 0) {
                //直接加载
                bitmapList.add(resource);
            } else {
                //说明需要切分图片
                for (int i = 0; i < sum; i++) {
                    //需要注意：mRect.set(left, top, right, bottom)的第四个参数，
                    //也就是图片的高不能大于这里的4096
                    mRect.set(0, i * maxHeight, imgWidth, (i + 1) * maxHeight);
                    Bitmap bm = decoder.decodeRegion(mRect, opts);
                    bitmapList.add(bm);
                }
                //将多余的不足3000的部分作为尾部拼接
                if (redundant > 0) {
                    mRect.set(0, sum * maxHeight, imgWidth, imgHeight);
                    Bitmap bm = decoder.decodeRegion(mRect, opts);
                    bitmapList.add(bm);
                }
            }
            Bitmap bigbitmap = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
            Canvas bigcanvas = new Canvas(bigbitmap);
            Paint paint = new Paint();
            int iHeight = 0;
            //将之前的bitmap取出来拼接成一个bitmap
            for (int i = 0; i < bitmapList.size(); i++) {
                Bitmap bmp = bitmapList.get(i);
                bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
                iHeight += bmp.getHeight();
                bmp.recycle();
                bmp = null;
            }
            imageView.setImageBitmap(bigbitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void loadLongPicture(@DrawableRes int resId, Bitmap.CompressFormat format, ImageView imageView) {
        int maxHeight = LONG_PICTURE_MAX_HEIGHT;
        Rect mRect = new Rect();
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BitmapFactory.decodeResource(imageView.getResources(), resId).compress(format, 100, baos);
            InputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            //BitmapRegionDecoder newInstance(InputStream is, boolean isShareable)
            //用于创建BitmapRegionDecoder，isBm表示输入流，只有jpeg和png图片才支持这种方式，
            // isShareable如果为true，那BitmapRegionDecoder会对输入流保持一个表面的引用，
            // 如果为false，那么它将会创建一个输入流的复制，并且一直使用它。即使为true，程序也有可能会创建一个输入流的深度复制。
            // 如果图片是逐步解码的，那么为true会降低图片的解码速度。如果路径下的图片不是支持的格式，那就会抛出异常
            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(isBm, true);
            final int imgWidth = decoder.getWidth();
            final int imgHeight = decoder.getHeight();
            BitmapFactory.Options opts = new BitmapFactory.Options();
            //计算图片要被切分成几个整块，
            // 如果sum=0 说明图片的长度不足3000px，不进行切分 直接添加
            // 如果sum>0 先添加整图，再添加多余的部分，否则多余的部分不足3000时底部会有空白
            int sum = imgHeight / maxHeight;
            int redundant = imgHeight % maxHeight;
            List<Bitmap> bitmapList = new ArrayList<>();
            //说明图片的长度 < maxHeight
            if (sum == 0) {
                //直接加载
                imageView.setImageResource(resId);
            } else {
                //说明需要切分图片
                for (int i = 0; i < sum; i++) {
                    //需要注意：mRect.set(left, top, right, bottom)的第四个参数，
                    //也就是图片的高不能大于这里的4096
                    mRect.set(0, i * maxHeight, imgWidth, (i + 1) * maxHeight);
                    Bitmap bm = decoder.decodeRegion(mRect, opts);
                    bitmapList.add(bm);
                }
                //将多余的不足3000的部分作为尾部拼接
                if (redundant > 0) {
                    mRect.set(0, sum * maxHeight, imgWidth, imgHeight);
                    Bitmap bm = decoder.decodeRegion(mRect, opts);
                    bitmapList.add(bm);
                }
            }
            Bitmap bigbitmap = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
            Canvas bigcanvas = new Canvas(bigbitmap);
            Paint paint = new Paint();
            int iHeight = 0;
            //将之前的bitmap取出来拼接成一个bitmap
            for (int i = 0; i < bitmapList.size(); i++) {
                Bitmap bmp = bitmapList.get(i);
                bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
                iHeight += bmp.getHeight();
                bmp.recycle();
                bmp = null;
            }
            imageView.setImageBitmap(bigbitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap capture(WebView webView) {
        Picture picture = webView.capturePicture();
        int width = picture.getWidth();
        int height = picture.getHeight();
//        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        picture.draw(canvas);
//        return bitmap;
        return capture(webView , width , height , false , Bitmap.Config.ARGB_8888) ;
    }

    public static Bitmap capture(View view, float width, float height, boolean scroll, Bitmap.Config config) {
        if (!view.isDrawingCacheEnabled()) {
            view.setDrawingCacheEnabled(true);
        }
        Bitmap bitmap = Bitmap.createBitmap((int) width, (int) height, config);
        bitmap.eraseColor(Color.WHITE);
        Canvas canvas = new Canvas(bitmap);
        int left = view.getLeft();
        int top = view.getTop();
        if (scroll) {
            left = view.getScrollX();
            top = view.getScrollY();
        }
        int status = canvas.save();
        canvas.translate(-left, -top);
        float scale = width / view.getWidth();
        canvas.scale(scale, scale, left, top);
        view.draw(canvas);
        canvas.restoreToCount(status);
        Paint alphaPaint = new Paint();
        alphaPaint.setColor(Color.TRANSPARENT);
        canvas.drawRect(0f, 0f, 1f, height, alphaPaint);
        canvas.drawRect(width - 1f, 0f, width, height, alphaPaint);
        canvas.drawRect(0f, 0f, width, 1f, alphaPaint);
        canvas.drawRect(0f, height - 1f, width, height, alphaPaint);
        canvas.setBitmap(null);
        return bitmap;
    }

    //缩小图片到制定长宽
    public static Bitmap scaleImage(Bitmap bm, int newWidth, int newHeight) {
        if (bm == null) {
            return null;
        }
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,
                true);
        if (bm != null & !bm.isRecycled()) {
            bm.recycle();
            bm = null;
        }
        return newbm;
    }

    //图片转YUV
    public static byte[] getYUVByBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        int size = width * height;

        int pixels[] = new int[size];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        byte[] data = rgb2YCbCr420(pixels, width, height);

        return data;
    }

    public static byte[] rgb2YCbCr420(int[] pixels, int width, int height) {
        int len = width * height;
        //yuv格式数组大小，y亮度占len长度，u,v各占len/4长度。
        byte[] yuv = new byte[len * 3 / 2];
        int y, u, v;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                //屏蔽ARGB的透明度值
                int rgb = pixels[i * width + j] & 0x00FFFFFF;
                //像素的颜色顺序为bgr，移位运算。
                int r = rgb & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb >> 16) & 0xFF;
                //套用公式
                y = ((66 * r + 129 * g + 25 * b + 128) >> 8) + 16;
                u = ((-38 * r - 74 * g + 112 * b + 128) >> 8) + 128;
                v = ((112 * r - 94 * g - 18 * b + 128) >> 8) + 128;

                y = y < 16 ? 16 : (y > 255 ? 255 : y);
                u = u < 0 ? 0 : (u > 255 ? 255 : u);
                v = v < 0 ? 0 : (v > 255 ? 255 : v);
                //赋值
                yuv[i * width + j] = (byte) y;
                yuv[len + (i >> 1) * width + (j & ~1) + 0] = (byte) u;
                yuv[len + +(i >> 1) * width + (j & ~1) + 1] = (byte) v;
            }
        }
        return yuv;
    }
}

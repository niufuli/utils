package com.zealcomm.app.util

import com.zealcomm.app.util.ExceptionUtil.getExceptionTraceString
import com.zealcomm.app.util.LogUtil.i
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * TODO 转换成 kotlin 后不好使了
 */
class TypeTokenImpl<T> {
    var type: Type? = null

    init {
//        type = object : TypeToken<T>() {}.type
//        type = getRealType()
        type = this.javaClass.getDeclaredField("type_variable").genericType
    }

    private var clazz: Class<T>? = null
    private val typeVariable: T? = null

    // 使用反射技术得到T的真实类型
    private fun getRealType(): Class<*>? {
        try {
            // 获取当前new的对象的泛型的父类类型
            val pt: ParameterizedType = this.javaClass.genericSuperclass as ParameterizedType
            // 获取第一个类型参数的真实类型
            clazz = pt.actualTypeArguments[0] as Class<T>?
        } catch (e: Exception) {
            i(getExceptionTraceString(e))
        }
        return clazz
    }
}
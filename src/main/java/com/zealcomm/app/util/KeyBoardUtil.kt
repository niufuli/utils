package com.zealcomm.app.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.zealcomm.app.util.LogUtil.i

/**
 * 打开或关闭软键盘
 */
object KeyBoardUtil {
    /**
     * 打卡软键盘
     *
     * @param mEditText <br></br>输入框
     * @param mContext  <br></br>上下文
     */
    fun openKeyboard(mEditText: EditText?, mContext: Context) {
        val imm = mContext
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(mEditText, InputMethodManager.RESULT_SHOWN)
        imm.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
    }

    /**
     * 关闭软键盘
     *
     * @param view     <br></br>输入框
     * @param mContext <br></br>上下文
     */
    fun closeKeyboard(view: View, mContext: Context) {
        val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun closeKeyboard(view: View) {
        try {
            val imm =
                view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            i(ExceptionUtil.getExceptionTraceString(e))
        }
    }
}
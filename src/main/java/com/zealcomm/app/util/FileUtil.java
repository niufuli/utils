package com.zealcomm.app.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;

import androidx.core.content.FileProvider;

import java.io.File;

public class FileUtil {
    public static File getFileByUri(Context context, Uri uri) {
        Context contextImpl = context.getApplicationContext() == null ? context : context.getApplicationContext();
        String path = null;

        // 4.2.2以后
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = contextImpl.getContentResolver().query(uri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            path = cursor.getString(columnIndex);
        }
        cursor.close();

        return new File(path);

//        return null;
    }

    // app 沙箱中的目录，如：/storage/emulated/0/Android/data/包名/files/Download
    // public static String DOWNLOAD_PATH = getDownloadPath();
    // sdcard 下的 download 目录
    public static String PUBLIC_DOWNLOAD_PATH = "";

    public static String getImageRealPathFromURI(Uri contentURI, Context context) {
        String result = "";
        int idx = 0;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            if (document_id.contains(":")) {
                String id = document_id.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + "=?";
                Cursor cursorByColon = context.getContentResolver().
                        query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
                int columnIndex = cursorByColon.getColumnIndex(column[0]);
                if (cursorByColon.moveToFirst()) {
                    result = cursorByColon.getString(columnIndex);
                }
                cursorByColon.close();
            } else {
                idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                result = cursor.getString(idx);
            }
            cursor.close();
        }
        return result;
    }

    public static void openAlbum(Activity activity) {
        String IMAGE_TYPE = "image/*";
//        int IMAGE_REQUEST_CODE = 0x102;
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(IMAGE_TYPE);
        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        }
        // activity.startActivityForResult(intent, IMAGE_CODE);
        activity.startActivity(intent);

    }

    public static void openAssignFolder(Activity activity, String path) {
        Context context = activity;
        File file = new File(path);
        if (null == file || !file.exists()) {
            ToastUtil.showShort(path + "不存在");
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//            StrictMode.setVmPolicy(builder.build());
//            builder.detectFileUriExposure();
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        // intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        // intent.setAction("android.intent.action.VIEW");
        Uri fileUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //申请权限
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //getUriForFile的第二个参数就是Manifest中的authorities
            fileUri = FileProvider.getUriForFile(context,
                    activity.getApplicationInfo().packageName + ".Download", file);
        } else {
            fileUri = Uri.fromFile(file);
        }

        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        intent.setDataAndType(fileUri, mimeType);
        // intent.setDataAndType(fileUri, "file/*");


        try {
            // BaseApplication.application.startActivity(intent);
            activity.startActivityForResult(Intent.createChooser(intent, "选择浏览工具"), 100);
        } catch (ActivityNotFoundException e) {
            LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
        }
    }

    /**
     * 获取文件的 MimeType
     *
     * @param file
     * @return
     */
    public static String getMimeType(File file) {
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
        return android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

//    private static String getDownloadPath() {
//        return TextUtils.isEmpty(DOWNLOAD_PATH) ?
//                BaseApplication.application.getExternalFilesDir(DIRECTORY_DOWNLOADS).getAbsolutePath() :
//                DOWNLOAD_PATH;
//    }

    private static void openFile(Context context, File f) {
        Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(f).toString());
        String mimeType = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        myIntent.setDataAndType(Uri.fromFile(f), mimeType);
        context.startActivity(myIntent);
    }

    public static String getRealPath(Context context, Uri uri) {
        String realPath = "";
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA},
                    null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        realPath = cursor.getString(index);
                        LogUtil.i("真实路径：" + realPath);
                    }
                }
                cursor.close();
            }
        }
        return realPath;
    }

    public static String getRealPath(String path) {
        String realPath = "";
        if (null != path) {

        }
        return realPath;
    }
}

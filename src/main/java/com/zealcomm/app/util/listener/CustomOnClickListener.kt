package com.zealcomm.app.util.listener

import android.view.View
import android.widget.EditText
import com.zealcomm.app.util.KeyBoardUtil.closeKeyboard
import java.util.*

abstract class CustomOnClickListener<T : Any> : View.OnClickListener {
    var t: T? = null
    private var firstTime: Long = 0
    private var secondTime: Long = 0
    private var onClickInterceptor: OnClickInterceptor? = null
    private val onClickInterceptors: MutableList<OnClickInterceptor> = ArrayList()

    constructor() {}
    constructor(onClickInterceptor: OnClickInterceptor?) {
        this.onClickInterceptor = onClickInterceptor
    }

    constructor(onClickInterceptors: List<OnClickInterceptor>?) {
        this.onClickInterceptors.addAll(onClickInterceptors!!)
    }

    constructor(t: T) {
        this.t = t
    }

    /**
     * 方便以后处理多次点击等问题
     *
     * @param v
     */
    override fun onClick(v: View) {
        this.onClick(v, t)
        if (null != v) {
            if (v is EditText) {
                // 当是输入框时，不要关闭软键盘
            } else {
                closeKeyboard(v, v.context)
            }
        }

        // 防止连续点击
        secondTime = System.currentTimeMillis()
        val temp: MutableList<OnClickInterceptor> = ArrayList()
        if (null != onClickInterceptor) {
            temp.add(onClickInterceptor!!)
        }
        temp.addAll(onClickInterceptors)
        if (secondTime - firstTime > 400) {
            firstTime = secondTime
            if (temp.size == 0) {
                onCustomClick(v)
            } else {
                for (o in temp) {
                    if (o.interceptor()) {
                        // 拦截到一个
                        afterInterceptor()
                        return
                    }
                }
                onCustomClick(v)
            }
        } else {
            firstTime = secondTime
        }
    }

    abstract fun onCustomClick(view: View?)
    fun onClick(view: View?, t: T?) {}
    fun afterInterceptor() {}

    /**
     * 只用于弹窗取消按钮主动调用
     *
     * @param view
     */
    fun onCancel(view: View?) {}

    fun setOnClickInterceptor(onClickInterceptor: OnClickInterceptor?) {
        this.onClickInterceptor = onClickInterceptor
    }

    interface OnClickInterceptor {
        /**
         * @return true : 拦截点击事件，false ：不拦截点击事件
         */
        fun interceptor(): Boolean
    }

    fun addOnClickInterceptor(onClickInterceptor: OnClickInterceptor) {
        onClickInterceptors.add(onClickInterceptor)
    }

    fun removeOnClickInterceptor(onClickInterceptor: OnClickInterceptor): Boolean {
        return onClickInterceptors.remove(onClickInterceptor)
    }
}
package com.zealcomm.app.util

import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zealcomm.app.util.ExceptionUtil.getExceptionTraceString
import com.zealcomm.app.util.LogUtil.i
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Type


object GsonUtil {

    @JvmStatic
    fun toJSONObject(string: String?): JSONObject {
        try {
            return JSONObject(string)
        } catch (e: JSONException) {
            i(getExceptionTraceString(e))
        }
        return JSONObject()
    }

    fun toMap(`object`: Any?): HashMap<*, *> {
        var map = hashMapOf<Any?, Any?>()
        try {
            val jsonObject = JSONObject(object2String(`object`))
            val iterator = jsonObject.keys()
            var key: String?
            while (iterator.hasNext()) {
                key = iterator.next()
                val ob = jsonObject[key]
                if (false && isJsonArray(ob.toString())) {
                    // TODO 递归,目前这个方法是用来给服务器传参的不会有 JsonArray ,暂不考虑
                } else {
                    // 添加到 map 中
                    map[key] = ob
                }
            }
        } catch (e: JSONException) {
            i(getExceptionTraceString(e))
        }
        return map
    }

    fun isJsonArray(`object`: String?): Boolean {
        return try {
            val jsonArray = JSONArray(`object`)
            jsonArray != null
        } catch (e: JSONException) {
            i("isJsonArray 发生了异常说明不是 jsonArray")
            i(getExceptionTraceString(e))
            false
        }
    }

    private val gson = Gson()

    //1、对象转string
    @JvmStatic
    fun object2String(o: Any?): String {
        try {
            return gson.toJson(o)
        }catch (e : Exception){
            i(getExceptionTraceString(e))
            return ""
        }
    }

    @JvmStatic
    fun <T> toObject(str: String, type: Type): T? {
        return if (TextUtils.isEmpty(str)) {
            null
        } else {
            try {
                gson.fromJson<T>(str, type)
            } catch (e: Exception) {
                i(getExceptionTraceString(e))
                null
            }
        }
    }

    @JvmStatic
    fun <T> toObject(str: String?, clazz: Class<T>): T? {
        return if (TextUtils.isEmpty(str)) {
            null
        } else {
            try {
                gson.fromJson(str, clazz)
            } catch (e: Exception) {
                i(getExceptionTraceString(e))
                null
            }
        }
    }

    @JvmStatic
    fun <T> getType(t : Class<T>): Type {
        return object : TypeToken<T>() {}.type
    }

}